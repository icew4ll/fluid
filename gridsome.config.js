module.exports = {
  siteName: "fluid",
  icon: {
    favicon: "./src/assets/favicon.png",
    touchicon: "./src/assets/favicon.png"
  },
  siteUrl: process.env.SITE_URL ? process.env.SITE_URL : "https://example.com",

  plugins: [
    {
      use: "@gridsome/vue-remark",
      options: {
        typeName: "Post",
        baseDir: "./posts",
        pathPrefix: "/posts",
        template: "./src/templates/Post.vue",
        includePaths: ["./src/sections"],
        plugins: ["@gridsome/remark-prismjs"],
        route: "/blog/:slug",
        refs: {
          tags: "Tag"
        },
        remark: {
          autolinkHeadings: {
            content: {
              type: "text",
              value: "#"
            }
          }
        }
      }
    },
    {
      use: "@gridsome/vue-remark",
      options: {
        typeName: "Tag",
        baseDir: "./tags",
        pathPrefix: "/tags",
        template: "./src/templates/Tag.vue"
      }
    },
    {
      use: "gridsome-plugin-tailwindcss",
      options: {
        tailwindConfig: "./tailwind.config.js",
        purgeConfig: {
          // Prevent purging of prism classes.
          whitelistPatternsChildren: [/token$/]
        }
      }
    },
    {
      use: "@gridsome/plugin-sitemap",
      options: {}
    }
  ]
};
